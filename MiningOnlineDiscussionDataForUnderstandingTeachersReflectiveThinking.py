from flask import Flask,render_template

app = Flask(__name__)


@app.route('/admin_home')
def admnhome():
    return render_template('admin_home.html')
@app.route('/Adminadddepartment')
def adminadddeptmnt():
    return render_template('Admin_add_department.html')
@app.route('/Adminadddliscussion')
def adminadddiscussion():
    return render_template('Admin_add_dliscussion.htm')
@app.route('/adminaddteacher')
def admnaddteacher():
    return render_template('admin_add_teacher.html')
@app.route('/Adminviewdepartment')
def Admnviewdepartment():
    return render_template('Admin_view_department.html')
@app.route('/project1')
def fn():
    return 'reflective thinking'
if __name__ == '__main__':
    app.run()
